<html>
<head>
<title>Generate Password for textbox using Php</title>

</head>
<body>
<form name="myform" method="post" action="">
    <table width="100%" border="0">

        <tr>
            <td>Password:</td>
            <td>
                <input name="row_password" type="text" id="pass" size="40">&nbsp;
                <input type="button" class="button" value="Generate" onClick="generate();" tabindex="2">
                <input type="button" class="button" value="show" onClick="show();" >
                <input type="button" class="button" value="hide" onClick="hide();">
            </td>
        </tr>
    </table>
</form>
<script type="text/javascript">
	function randomPassword(length=8) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}

function generate() {
    myform.row_password.value = randomPassword(8);
}
function show(){
	document.getElementById('pass').type = 'text';
}
function hide(){
	document.getElementById('pass').type = 'password';
}
</script>
</body>
</html>
